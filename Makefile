CC          =   clang++
CPPFLAGS    =   -Wall -Wextra -std=c++11 -pedantic -g3

TARGET      =   nn

SRC         =   $(addprefix src/,       \
                  input-layer.cc        \
                  output-layer.cc       \
                  intern-layer.cc       \
                  neuron.cc             \
                  network.cc            \
                  main.cc               \
                  )

$(TARGET): $(SRC)
	$(CC) $(CPPFLAGS) -o $(TARGET) $(SRC)

clean:
	rm $(TARGET)
