#include "input-layer.hh"

InputLayer::InputLayer(int size)
           : size_(size)
{
  inputs_ = new std::vector<float>();
}

InputLayer::~InputLayer()
{
  delete inputs_;
}

void InputLayer::addInput(float in)
{
  inputs_->push_back(in);
}

void InputLayer::resetInput()
{
  delete inputs_;
  inputs_ = new std::vector<float>();
}
