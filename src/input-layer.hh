#ifndef INPUT_LAYER_HH
# define INPUT_LAYER_HH

# include "neuron.hh"

class InputLayer
{
  public:
    InputLayer(int size);
    ~InputLayer();

  public:
    void addInput(float in);
    void resetInput();

  private:
    int size_;
    std::vector<float>* inputs_;
};

#endif /* INPUT_LAYER_HH */
