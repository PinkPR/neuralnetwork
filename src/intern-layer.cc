#include "intern-layer.hh"

InternLayer::InternLayer(int size)
            : size_(size)
{
  neurons_ = new std::vector<Neuron*>();
  outputs_ = new std::vector<float>();
}

InternLayer::~InternLayer()
{
  for (int i = 0; i < neurons_->size(); i++)
    delete neurons_->at(i);
  delete neurons_;
  delete outputs_;
}

void InternLayer::addNeuron(Neuron* neuron)
{
  neurons_->push_back(neuron);
}

void InternLayer::compute_outputs(std::vector<float>* inputs)
{
  if (outputs_ && outputs_->size())
  {
    delete outputs_;
    outputs_ = new std::vector<float>();
  }

  for (int i = 0; i < neurons_->size(); i++)
  {
    outputs_->push_back(neurons_->at(i)->getOutput(inputs));
  }
}

std::vector<float>* InternLayer::getOutputs()
{
  return outputs_;
}

void InternLayer::randomizeNeurons(int in_count)
{
  Neuron* neuron = nullptr;

  for (int i = 0; i < size_; i++)
  {
    neuron = new Neuron(in_count);
    neuron->randomizeWeights();
    addNeuron(neuron);
  }
}

std::vector<Neuron*>* InternLayer::getNeurons()
{
  return neurons_;
}

int InternLayer::getSize()
{
  return size_;
}
