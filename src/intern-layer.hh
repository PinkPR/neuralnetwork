#ifndef INTERN_LAYER_HH
# define INTERN_LAYER_HH

# include "neuron.hh"

class InternLayer
{
  public:
    InternLayer(int size);
    ~InternLayer();

  public:
    void addNeuron(Neuron* neuron);
    void compute_outputs(std::vector<float>* inputs);
    std::vector<float>* getOutputs();
    void randomizeNeurons(int in_count);
    std::vector<Neuron*>* getNeurons();
    int getSize();

  private:
    int size_;
    std::vector<Neuron*>* neurons_;
    std::vector<float>* outputs_;
};

#endif /* INTERN_LAYER_HH */
