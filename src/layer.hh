#ifndef INTERN_LAYER_HH
# define INTERN_LAYER_HH

# include "neuron.hh"

class InternLayer
{
  public:
    InternLayer(int size);
    ~InternLayer();

  public:
    void compute_outputs();

  private:
    int size_;
    std::vector<Neuron>* neurons_;
    std::vector<float>* outputs_;
}

#endif /* INTERN_LAYER_HH */
