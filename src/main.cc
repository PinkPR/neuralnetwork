#include "network.hh"

#include <iostream>

void run_test(Network* network, float f1)//, float f2)
{
  std::vector<float>* inputs = new std::vector<float>();

  inputs->push_back(f1);
  //inputs->push_back(f2);

  std::cout << "Network result : "
            << f1
            //<< " "
            //<< f2
            << " "
            << network->getOutputNeuron(inputs)
            << std::endl;

  delete inputs;
}

int main()
{
  Network* network = new Network(1);
  network->addInternLayer(1);
  network->setOutputLayer(25);
  network->randomizeNetwork();

  int nb = 0;

  for (int i = 0; i < 1000; i++)
  {
    for (nb = 0; nb < 25; nb++)
    {
      run_test(network, float(nb));
      network->train(nb, 1.0f, 0.1f);
    }

    /*run_test(network, 0.0f, 0.0f);
    network->train(0, 1.0f, 0.1f);

    run_test(network, 0.0f, 1.0f);
    network->train(1, 1.0f, 0.1f);

    run_test(network, 1.0f, 0.0f);
    network->train(2, 1.0f, 0.1f);

    run_test(network, 1.0f, 1.0f);
    network->train(3, 1.0f, 0.1f);*/
  }

  std::cout << "After training :" << std::endl;

  /*run_test(network, 0.0f, 0.0f);
  run_test(network, 0.0f, 1.0f);
  run_test(network, 1.0f, 0.0f);
  run_test(network, 1.0f, 1.0f);*/

  for (int i = 0; i < 25; i++)
    run_test(network, float(i));

  return 0;
}
