#include "network.hh"
#include <iostream>

Network::Network(int input_size)
        : input_size_(input_size)
        , intern_layers_(nullptr)
        , input_layer_(nullptr)
        , output_layer_(nullptr)
{
}

Network::~Network()
{
  if (intern_layers_)
  {
    for (int i = 0; i < intern_layers_->size(); i++)
      delete intern_layers_->at(i);
    delete intern_layers_;
  }
  if (input_layer_)
    delete input_layer_;
  if (output_layer_)
    delete output_layer_;
}

void Network::setInputLayer(int size)
{
  input_layer_ = new InputLayer(size);
}

void Network::setOutputLayer(int size)
{
  output_layer_ = new OutputLayer(size);
}

void Network::addInternLayer(int size)
{
  if (intern_layers_ == nullptr)
    intern_layers_ = new std::vector<InternLayer*>();

  InternLayer* intern = new InternLayer(size);
  intern_layers_->push_back(intern);
}

int Network::getOutputNeuron(std::vector<float>* inputs)
{
  if (intern_layers_ != nullptr)
  {
    intern_layers_->at(0)->compute_outputs(inputs);
    std::vector<float>* outputs = intern_layers_->at(0)->getOutputs();

    for (int i = 1; i < intern_layers_->size(); i++)
    {
      intern_layers_->at(i)->compute_outputs(outputs);
      outputs = intern_layers_->at(i)->getOutputs();
    }

    output_layer_->compute_outputs(outputs);
  }
  else
    output_layer_->compute_outputs(inputs);

  return output_layer_->getOutputNeuron();
}

void Network::randomizeNetwork()
{
  int in_size = input_size_;

  if (intern_layers_ != nullptr)
  {
    for (int i = 0; i < intern_layers_->size(); i++)
    {
      intern_layers_->at(i)->randomizeNeurons(in_size);
      in_size = intern_layers_->at(i)->getSize();
    }
  }

  output_layer_->randomizeNeurons(in_size);
}

/*float Network::computeInternError(std::vector<float>* weights,
                                  Neuron* neuron,
                                  float ex_error)
{
}*/

std::vector<float> Network::getOutputWeightsO(OutputLayer* output,
                                              int in_i)
{
  std::vector<float> out_weights;

  for (int i = 0; i < output->getSize(); i++)
    out_weights.push_back(
      output->getNeurons()->at(i)->getWeight(in_i));

  return out_weights;
}

std::vector<float> Network::getOutputWeightsI(InternLayer* output,
                                              int in_i)
{
  std::vector<float> out_weights;

  for (int i = 0; i < output->getSize(); i++)
    out_weights.push_back(
      output->getNeurons()->at(i)->getExWeights().at(in_i));

  return out_weights;
}

std::vector<float> Network::getErrorsO(OutputLayer* output)
{
  std::vector<float> errors;

  for (int i = 0; i < output->getSize(); i++)
    errors.push_back(output->getNeurons()->at(i)->getError());

  return errors;
}

void Network::train(int result, float expected, float n)
{
  std::vector<float>* outputs = output_layer_->getOutputs();

  output_layer_->train(result, expected, n);

  std::vector<float> outw;

  for (int i = 0; i < intern_layers_->at(0)->getSize(); i++)
  {
    outw = getOutputWeightsO(output_layer_, i);
    intern_layers_->at(0)->getNeurons()->at(i)->trainHidden(n,
                                                            outw,
                                                            getErrorsO(output_layer_));
  }

  /*for (int i = intern_layers_->size(); i > 0; i--)
  {
    InternLayer* current = intern_layers_->at(i - 1);

    for (int j = 0; j < current->getSize(); j++)
    {
    }
  }*/

}
