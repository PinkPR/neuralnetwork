#ifndef NETWORK_HH
# define NETWORK_HH

# include "neuron.hh"
# include "intern-layer.hh"
# include "input-layer.hh"
# include "output-layer.hh"

class Network
{
  public:
    Network(int input_size);
    ~Network();

  public:
    void setInputLayer(int size);
    void setOutputLayer(int size);
    void addInternLayer(int size);
    int getOutputNeuron(std::vector<float>* inputs);
    void randomizeNetwork();
    std::vector<float> getOutputWeightsO(OutputLayer* output,
                                         int in_i);
    std::vector<float> getOutputWeightsI(InternLayer* output,
                                         int in_i);
    std::vector<float> getErrorsO(OutputLayer* output);
    void train(int result, float expected, float n);

  private:
    int input_size_;
    std::vector<InternLayer*>* intern_layers_;
    InputLayer* input_layer_;
    OutputLayer* output_layer_;
};

#endif /* NETWORK_HH */
