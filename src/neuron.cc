#include "neuron.hh"

Neuron::Neuron(int input_count)
       : input_count_(input_count)
       , sum_(0.0f)
       , output_(0.0f)
       , error_(0.0f)
{
  in_weights_ = new std::vector<float>();
}

Neuron::~Neuron()
{
  delete in_weights_;
}

float Neuron::getOutput(std::vector<float>* inputs)
{
  float out = getSum(inputs);

  out = 1.0f / (1.0f + exp(-out));

  if (out < 0.05f)
    return 0.0f;

  output_ = out;

  return out;
}

void Neuron::setNextWeight(float w)
{
  in_weights_->push_back(w);
  ex_weights_.push_back(w);
}

float Neuron::getWeight(int i)
{
  return in_weights_->at(i);
}

void Neuron::randomizeWeights()
{
  for (int i = 0; i < input_count_; i++)
    setNextWeight(float(rand() % 1000) / 1000.0f);
}

void Neuron::trainIntern(std::vector<float> weights,
                         std::vector<float> errors,
                         float step)
{
  error_ = computeGeneralError(weights, errors);

  train(error_, step);
}

float Neuron::getDelta(float error, float input, float step)
{
  return step * error * input;
}

void Neuron::updateWeights(float step)
{
  for (int i = 0; i < in_weights_->size(); i++)
    in_weights_->at(i) += getDelta(error_, inputs_.at(i), step);
}

void Neuron::trainOutput(float expected, float step)
{
  error_ = output_ * (1.0f - output_) * (expected - output_);

  updateWeights(step);
}

void Neuron::trainHidden(float step,
                         std::vector<float> weights,
                         std::vector<float> errors)
{
  error_ = 0.0f;

  for (int i = 0; i < weights.size(); i++)
    error_ += weights.at(i) * errors.at(i);

  error_ *= output_ * (1.0f - output_);

  updateWeights(step);
}

void Neuron::train(float error, float step)
{
  error_ = error;

  for (int i = 0; i < in_weights_->size(); i++)
    in_weights_->at(i) += step * error;
}

float Neuron::getSum(std::vector<float>* inputs)
{
  inputs_ = *inputs;
  float out = 0.0f;

  for (int i = 0; i < input_count_; i++)
    out += inputs->at(i) * in_weights_->at(i);

  sum_ = out;

  return out;
}

std::vector<float> Neuron::getExWeights()
{
  return ex_weights_;
}

float Neuron::getOutput()
{
  return output_;
}

float Neuron::computePartialError(std::vector<float> weights,
                                  float error,
                                  int in)
{
  float sum = 0.0f;

  for (int i = 0; i < weights.size(); i++)
    sum += weights.at(i);

  return (error * weights.at(in)) / sum;
}

float Neuron::computeGeneralError(std::vector<float> weights,
                                  std::vector<float> errors)
{
  float sum = 0.0f;

  for (int i = 0; i < errors.size(); i++)
    sum += computePartialError(weights, errors.at(i), i);

  return sum;
}

float Neuron::getError()
{
  return error_;
}

void Neuron::printWeights()
{
  for (int i = 0; i < input_count_; i++)
  {
    std::cout << "w["
              << i
              << "] "
              << in_weights_->at(i)
              << std::endl;
  }
}
