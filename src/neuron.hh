#ifndef NEURON_HH
# define NEURON_HH

# include <vector>
# include <cmath>
# include <cstdlib>
# include <iostream>

class Neuron
{
  public:
    Neuron(int input_count);
    ~Neuron();

  public:
    float getOutput(std::vector<float>* inputs);
    void setNextWeight(float w);
    float getWeight(int i);
    void randomizeWeights();
    void trainIntern(std::vector<float> weights,
                     std::vector<float> errors,
                     float step);
    float getDelta(float error, float input, float step);
    void updateWeights(float step);
    void trainOutput(float expected, float step);
    void trainHidden(float step,
                     std::vector<float> weights,
                     std::vector<float> errors);
    void train(float error, float n);
    std::vector<float> getExWeights();
    float computePartialError(std::vector<float> weights,
                              float error,
                              int in);
    float computeGeneralError(std::vector<float> weights,
                              std::vector<float> errors);
    float getOutput();
    float getError();

  private:
    float getSum(std::vector<float>* inputs);
    void printWeights();

  private:
    int input_count_;
    float sum_;
    float output_;
    float error_;
    std::vector<float>* in_weights_;
    std::vector<float> ex_weights_;
    std::vector<float> inputs_;
};

#endif /* NEURON_HH */
