#include "output-layer.hh"

OutputLayer::OutputLayer(int size)
            : size_(size)
{
  neurons_ = new std::vector<Neuron*>();
  outputs_ = new std::vector<float>();
}

OutputLayer::~OutputLayer()
{
  for (int i = 0; i < neurons_->size(); i++)
    delete neurons_->at(i);
  delete neurons_;
  delete outputs_;
}

void OutputLayer::addNeuron(Neuron* neuron)
{
  neurons_->push_back(neuron);
}

void OutputLayer::compute_outputs(std::vector<float>* inputs)
{
  if (outputs_ && outputs_->size())
  {
    delete outputs_;
    outputs_ = new std::vector<float>();
  }

  for (int i = 0; i < size_; i++)
  {
    outputs_->push_back(neurons_->at(i)->getOutput(inputs));
  }
}

int OutputLayer::getOutputNeuron()
{
  float max = 0.0f;
  int i = 0;

  for (int j = 0; j < size_; j++)
  {
    if (outputs_->at(j) > max)
    {
      max = outputs_->at(j);
      i = j;
    }
  }

  return i;
}

std::vector<float>* OutputLayer::getOutputs()
{
  return outputs_;
}

void OutputLayer::randomizeNeurons(int in_count)
{
  Neuron* neuron = nullptr;

  for (int i = 0; i < size_; i++)
  {
    neuron = new Neuron(in_count);
    neuron->randomizeWeights();
    addNeuron(neuron);
  }
}

std::vector<Neuron*>* OutputLayer::getNeurons()
{
  return neurons_;
}

int OutputLayer::getSize()
{
  return size_;
}

void OutputLayer::train(int target, float expected, float step)
{
  for (int i = 0; i < neurons_->size(); i++)
  {
    if (i == target)
      neurons_->at(i)->trainOutput(expected, step);
    else
      neurons_->at(i)->trainOutput(1.0f - expected, step);
  }
}
