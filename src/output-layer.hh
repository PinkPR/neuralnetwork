#ifndef OUTPUT_LAYER_HH
# define OUTPUT_LAYER_HH

# include "neuron.hh"

class OutputLayer
{
  public:
    OutputLayer(int size);
    ~OutputLayer();

  public:
    void addNeuron(Neuron* neuron);
    void compute_outputs(std::vector<float>* inputs);
    int getOutputNeuron();
    void randomizeNeurons(int in_count);
    std::vector<float>* getOutputs();
    std::vector<Neuron*>* getNeurons();
    int getSize();
    void train(int i, float expected, float n);

  private:
    int size_;
    std::vector<Neuron*>* neurons_;
    std::vector<float>* outputs_;
};

#endif /* OUTPUT_LAYER_HH */
